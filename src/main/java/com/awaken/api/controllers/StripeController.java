package com.awaken.api.controllers;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import com.stripe.model.checkout.Session;
import com.stripe.param.PaymentIntentCreateParams;
import com.stripe.param.checkout.SessionCreateParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/stripe")
public class StripeController {

    private static final Logger logger = LoggerFactory.getLogger(StripeController.class);

    private static final String publishableKey="pk_test_51PRh3UGTG3KNzVItX4Y0fbknQUg6TpHOmuPUbVpiJ6NtL5h7A7glHDDzZpuNQ0VF6UuKJvzmwxLj0WVXnnpgVc6z004VGI7kp2";
    private static final String secretKey="sk_test_51PRh3UGTG3KNzVItTaKKgXBolqV4lxRsnmG43i312QuqRFNdRBCo57LSSi1xtz37aBL8e4KxIXRwWo2riCLbhy0V00U9xm9hJj";

    static {
        Stripe.apiKey = secretKey;
    }

    @PostMapping("/create-checkout-session")
    @ResponseBody
    public Map<String, String> createCheckoutSession() throws StripeException {
        SessionCreateParams params = SessionCreateParams.builder()
                .setMode(SessionCreateParams.Mode.PAYMENT)
                .setUiMode(SessionCreateParams.UiMode.EMBEDDED)
                .setReturnUrl("http://localhost:3000/#/accueil?session_id={CHECKOUT_SESSION_ID}")
                .addLineItem(
                        SessionCreateParams.LineItem.builder()
                                .setQuantity(1L)
                                .setPriceData(
                                        SessionCreateParams.LineItem.PriceData.builder()
                                                .setCurrency("eur")
                                                .setUnitAmount(2000L)
                                                .setProductData(
                                                        SessionCreateParams.LineItem.PriceData.ProductData.builder()
                                                                .setName("T-shirt")
                                                                .build())
                                                .build())
                                .build())
                .build();

        Session session = Session.create(params);

        Map<String, String> map = new HashMap<>();
        map.put("clientSecret", session.getClientSecret());

        return map;
    }

    @PostMapping("/stripe-secret-key")
    @ResponseBody
    public Map<String, String>  getStripeSecretKey(@RequestBody Map<String, Object> payload) throws StripeException {
        try {
            Long amount = ((Number) payload.get("total")).longValue();
            PaymentIntentCreateParams createParams = new PaymentIntentCreateParams.Builder()
                    .setAmount(amount)
                    .setCurrency("eur")
                    .build();
            PaymentIntent intent = PaymentIntent.create(createParams);
            Map<String, String> map = new HashMap<>();
            map.put("clientSecret", intent.getClientSecret());
            return map;
        } catch (StripeException e) {
            return null;
        }
    }
}
