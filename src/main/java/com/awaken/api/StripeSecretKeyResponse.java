package com.awaken.api;

public class StripeSecretKeyResponse {
    private final String secretKey;
    private final String publishableKey;

    public StripeSecretKeyResponse(String secretKey, String publishableKey) {
        this.secretKey = secretKey;
        this.publishableKey = publishableKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String getPublishableKey() {
        return publishableKey;
    }
}
